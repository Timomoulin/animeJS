let elementsToTranslate = document.querySelectorAll('.translate');
let elementsToChange= document.querySelectorAll(".modifierCSS");
let inputCompter=document.querySelectorAll(".compter");

//plus d'infos https://animejs.com/documentation/#specificUnitValue

//translation simple
anime({
  targets: elementsToTranslate, //La cible
  translateX: 270,
  delay: 1000 //Declencheur debute aprés x ms
});

//changement de style css
anime({
    targets: elementsToChange,
    left: '240px',
    backgroundColor: '#FFF',
    borderRadius: ['0%', '50%'],
    easing: 'easeInOutQuad',
    delay :1000,
    duration: 3000 //Le temps que prend l'annimation pour se terminer
    
  });

  //modifier la valeur d'un input
  anime({
    targets: inputCompter,
    value: [0, 9999],
    round: 1,
    easing: 'easeInOutExpo',
    duration:2000
  });

  anime({
    targets: '.retour', //on peut placer directement un selecteur css en ""
    translateX: 250,
    endDelay: 1500,
    direction: 'alternate'
  });

  anime({
    targets: '.enMouvement',
    translateX: 270,
    direction: 'alternate',
    loop: true,
    delay: function(el, i, l) {
      return i * 300;
    },
    endDelay: function(el, i, l) {
      return (l - i) * 150;
    }
  });

  anime({
    targets: '.keyFrames',
    keyframes: [
      {translateY: -10},
      {backgroundColor:"#FF0000"},
      {translateX: 500},
      {translateY: 100},
      {backgroundColor:"#00FFFF"},
      {scale:0},
      {translateX: -100},
      {scale:1},
      {translateY: 0},
      {translateX: 0}
    ],
    duration: 4000,
    easing: 'easeOutElastic(1, .8)',
    loop: true
  });